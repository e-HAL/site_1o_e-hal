# 1º e-HAL

Este é o repositório do site do [Primeiro Encontro Brasileiro de Hardware Aberto e Livre](https://hardwareaberto.org/e-hal).

É baseado no tema [Hydejack](http://qwtel.com/hydejack/2016/03/08/introducing-hydejack/).

Para contribuir diretamente neste repositório é recomendável que [conheça e instale o Jekyll](https://jekyllrb.com/docs/quickstart/).
