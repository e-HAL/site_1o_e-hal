---
layout: page
title: Programação
image: ../public/img/Demoiselle_Drawing_b.jpg
---

__Esta programação é preliminar e está sujeita à alterações.__

## Dia 1: Sábado 29 de Outubro

#### Manhã
* 9:00 Recepção / Sessão Física e Música
* 10:00 Roda de conversa sobre HAL
* 11:00 Orientações iniciais Hackatona de Libertação de Hardware
* 11:30 Inscrição de projetos e formação de equipes (Hackatona)
* 12:00 Discussão nos grupos e rodada de relatos (Hackatona)

* 13:00 Almoço

#### Tarde
* 14:00 Mesa Redonda 1: HAL para ciência e educação
* 16:00 Oficina Arduino (Sala 1)
* 16:00 Oficina KiCAD (Sala 2)
* 16:00 Oficina Fabricação digital (Sala 3)

* 18:00 Palestra plenária: Javier Serrano
* 20:00 Física e Música
* 20:30 Encerramento

## Dia 2: Domingo 30 de Outubro

#### Manhã
* 9:00 Oficina Arduino (Sala 1)
* 9:00 Oficina KiCAD (Sala 2)
* 9:00 Oficina Fabricação digital (Sala 3)
* 11:30 Mesa Redonda: Gênero na ciência e tecnologia

* 13:00 Almoço

#### Tarde
* 14:00 Mesa redonda: Aspectos sociais de HAL
* 16:00 Oficina Arduino (Sala 1)
* 16:00 Oficina KiCAD (Sala 2)
* 16:00 Oficina Fabricação digital (Sala 3)
* 17:00 Fechamento da hackatona de libertação de Hardware (Hackatona)
* 18:00 Elaboração de Manifesto HAL no Brasil


## Dia 3: Segunda-feira 31 de Outubro (CCSL/USP)

#### Manhã
* 9:00 Palestra 1 (A definir)
* 10:00 Mesa redonda: HAL e inovação aberta no Brasil

* 12:00 Almoço

#### Tarde
* 13:00 Palestra 2 (A definir)
* 13:45 Palestra 3 (A definir)
* 15:00 Oficina de documentação e licenciamento de projetos livres e abertos

* 18:00 Encerramento
