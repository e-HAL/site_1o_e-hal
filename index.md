---
layout: default
title: Boas Vindas
---

# Primeiro Encontro Brasileiro de Hardware Aberto e Livre

## De 29 a 31 de Outubro de 2016, São Paulo, SP

### Atenção: site em construção! Fique atento para novidades!
 O Primeiro Encontro Brasileiro de Hardware Aberto e Livre, e-HAL, é uma conferência que visa celebrar o conhecimento livre pelo fortalecimento e difusão da cultura do hardware aberto e livre (HAL) tanto em no contexto acadêmico como no setor produtivo. São 3 dias para conversas e atividades práticas de disseminação de técnicas e práticas para colaboração e difusão de projetos abertos. O e-HAL também será palco da Hackatona de Libertação de Hardware com atividades que de palestras e de oficinas que evidenciam a o papel central da documentação de projetos de hardware. Confira a programação com atividades para aprofundar a discussão sobre HAL em instituições de pesquisa, universidades e empresas.

 O tema deste site é uma homenagem a [Santos Dumont](homenagem), pioneiro da aviação e das tecnologias livres.

### O que é HAL?
Hardware aberto e livre (HAL) é aquele que possui documentação com os elementos necessários que permitam a qualquer um estudar, modificar, distribuir, fabricar e vender o projeto ou o hardware baseado no projeto, veja [definição de Hardware de Código Aberto](http://hardwareaberto.org/wp/definicao-de-hardware-aberto/). Enquanto a importância da documentação (código fonte) de HAL está clara no contexto internacional, a cultura de documentação de projetos ainda é incipiente no Brasil. Esperamos que este evento seja um marco para o movimento de hardware aberto e livre no Brasil.

Além das questões específicas do HAL, o evento tem como pauta o protagonismo feminino na ciência e tecnologia (ou a falta dele). Acreditamos que cultura do compartilhamento e as tecnologias livres são revolucionárias e podem ser utilizadas como ferramentas para a quebra de paradigmas, auxiliando na construção de uma sociedade mais justa. Esse avanço deve ser construído por todes, e não apenas pelos grupos privilegiados. Motivados por isso, defendemos que a discussão da falta de protagonismo de grupos minorizados na construção desta cultura deve ser abordada no evento. Para isto o evento se dispõe a conduzir discussões de cunho feminista por meio de palestras e rodas de conversas, além de garantir que o evento seja permeado pelo protagonismo feminino.

# Destaques

## Palestrantes convidados
* Javier Serrano (CERN)

## Oficinas
* KiCad para projetos eletrônicos
* OpenSCAD para projeto de hardware
* Oficina de solda
* GIT para projetos de hardware
* Dojo Arduino

## Mesas redondas
* HAL para educação
* Aspectos legais de HAL
* Aspectos sociais de HAL
* Gênero na ciência e tecnologia
* HAL e inovação aberta no Brasil


### Quando e onde?

O e-HAL ocorre de 29 a 31 de Outubro de 2016 em São Paulo, SP em duas etapas:

### 29 e 30 de Outubro (Sábado e Domingo) na Pontifícia Universidade Católica de São Paulo (PUC-SP);

### 31 de Outubro (Segunda-feira) no Centro de Competência em Software Livre (CCSL), USP.

## Apoio

### [Pontifícia Universidade Católica de São Paulo](http://www.pucsp.br/)

  ![Pontifícia Universidade Católica de São Paulo](/e-hal/public/img/Brasao-PUCSP.png)

### [Centro de Competência em Software Livre/USP](http://ccsl.ime.usp.br/)

  ![Centro de Competência em Software Livre/USP](/e-hal/public/img/ccsl.png)

### [Projeto Meninas na Ciência e Tecnologia IF/UFRGS](http://www.ufrgs.br/meninasnaciencia)

  ![Logo Meninas na Ciência](/e-hal/public/img/meninas.png)
