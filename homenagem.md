---
layout: page
title: Homenagem
image: ../public/img/001i_portrait_santos_dumont_350.png
#color: '#949667'
---

## Santos Dumont

O tema deste site foi inspirado em Santos Dumont, pioneiro da aviação e das tecnologias livres.

Disponibilizou a patente das suas invenções para uso livre, sendo destacado na revista Popular Mechanics de Dezembro de 1909.

>A manchete da “Popular Mechanics” era clara: “Santos Dumont doa patentes de aeroplano ao mundo”. No texto, explicita que “Os direitos de patente da máquina foram tornados públicos por Santos Dumont para encorajar a aviação, e qualquer um pode usar esses planos para construir uma máquina similar”.

Fonte: [Revista Popular Mechanics, Dezembro de 1909](https://archive.org/download/PopularMechanics1909/Popular_Mechanics_12_1909.pdf), página 842.

Ver também especial [O Globo. Santos Dumont, o conquistador dos céus](http://infograficos.oglobo.globo.com/sociedade/santos-dumont.html).


Imagens em domínio público.
