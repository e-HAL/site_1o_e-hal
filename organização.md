---
layout: page
title: Organização
image: ../public/img/draw-18-demouselle_b.jpg
---

### [Centro de Tecnologia Acadêmica IF/UFRGS](http://cta.if.ufrgs.br)
Rafael Pezzi, Marina de Freitas, Leonardo Sehn, Jan Luc Tavares, Renan Silva

  ![Logo CTA](/e-hal/public/img/logoCTA_350x136.png)

### [Laboratório Nacional de Luz Síncrotron](http://www.lnls.br)
Daniel Tavares

  ![Logo LNLS](/e-hal/public/img/logoLNLS.jpg)

### [Garoa Hacker Clube](https://garoa.net.br)
Eduardo Oda, Gustavo Bruno Aylons

  ![Logo Garoa](/e-hal/public/img/171px-Logo_GaroaHC_verde.png)

# Apoio

### [Pontifícia Universidade Católica de São Paulo](http://www.pucsp.br/)

  ![Pontifícia Universidade Católica de São Paulo](/e-hal/public/img/Brasao-PUCSP.png)

### [Centro de Competência em Software Livre/USP](http://ccsl.ime.usp.br/)

  ![Centro de Competência em Software Livre/USP](/e-hal/public/img/ccsl.png)

### Projeto Meninas na Ciência e Tecnologia IF/UFRGS: [Site](http://www.ufrgs.br/meninasnaciencia) e [Facebook](https://facebook.com/meninasnacienciaufrgs/)

  ![Logo Meninas na Ciência](/e-hal/public/img/meninas.png)

## Links

* [Código fonte deste site](https://gitlab.com/e-HAL/site_1o_e-hal)
